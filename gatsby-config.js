module.exports = {
  siteMetadata: {
    title: `Telesan`,
    description: `Red de telesalud que conecta los servicios médicos de las comunidades remotas 
    con los servicios de especialidades médicas en otras partes de Honduras`,
    author: `gaby tinoco`,
    image: "/src/assets/logoTelesan.png"
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets`,
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/telesan-icon.png`, // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
    `gatsby-plugin-fontawesome-css`,
    
  ]
}
