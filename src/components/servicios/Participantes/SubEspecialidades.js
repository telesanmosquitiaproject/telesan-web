import React from "react"
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
  }));


const SubEspecialidades = () => {
    const classes = useStyles();
    return (
        <div id = "subespecialidades">
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <Typography className={classes.heading}>Subespecialidades(10)</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography>
                    <ul>
                        <li>Nefrología adultos</li>
                        <li>Endocrinología Pediátrica</li>
                        <li>Neurología Pediátrica</li>
                        <li>Dermatología Pediátrica</li> 
                        <li>Cardiología Pediátrica</li>
                        <li>Oncología de adultos</li>
                        <li>Coloproctología</li>
                        <li>Reumatología</li>
                        <li>Endocrinología de adultos</li>
                        <li>Pediatría intensiva</li>
                    </ul>
                </Typography>
            </AccordionDetails>
        </Accordion>
        <Accordion>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel2a-content"
                id="panel2a-header"
            >
                <Typography className={classes.heading}>Carreras (3)</Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography>
                    <ul>
                        <li>Nutrición</li>
                        <li>Terapia Física y Rehabilitación (TeleEducación)</li>
                        <li>Psicología</li>
                    </ul>
                </Typography>
            </AccordionDetails>
            </Accordion>
      </div>
    )
}

export default SubEspecialidades;