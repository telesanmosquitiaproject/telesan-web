import React from 'react';

const Footer =  ({ siteTitle }) => {

  return (
    <footer className="footer">
      <div className="footer-belt">
        <div className="company-sign">
        &copy; 2021 {siteTitle}
      </div>
        <div className="social-media">
          <a href="https://www.facebook.com/TELESAN-Honduras-100759555532942" 
          target= "_blank" rel = "noreferrer"
          ><div className="social-facebook">Facebook</div></a>
          <a href="https://www.instagram.com/telesan.hn/" target= "_blank" rel="noreferrer"
          ><div className="social-instagram">Instagram</div></a>
        </div>
      </div>
    </footer>
  )
};

export default Footer;
