import React from "react"
import axios from "axios"
import * as qs from "query-string"

class Formulario extends React.Component {
   constructor(props) {
    super(props)
    this.domRef = React.createRef()
    this.state = { feedbackMsg: null }
   }

   handleSubmit(event) {
   // Do not submit form via HTTP, since we're doing that via XHR request.
   event.preventDefault()
   // Loop through this component's refs (the fields) and add them to the
   // formData object. What we're left with is an object of key-value pairs
   // that represent the form data we want to send to Netlify.
   const formData = {}
   Object.keys(this.refs).map(key => (formData[key] = this.refs[key].value))
 
   // Set options for axios. The URL we're submitting to
   // (this.props.location.pathname) is the current page.
   console.info(this.props.location.pathname);
   const axiosOptions = {
     url: "/plataforma/",
     method: "post",
     headers: { "Content-Type": "application/x-www-form-urlencoded" },
     data: qs.stringify(formData),
   }
 
   // Submit to Netlify. Upon success, set the feedback message and clear all
   // the fields within the form. Upon failure, keep the fields as they are,
   // but set the feedback message to show the error state.
   axios(axiosOptions)
     .then(response => {
       this.setState({
         feedbackMsg: "Datos enviados con éxito",
       })
       this.domRef.current.reset()
     })
     .catch(err =>
       this.setState({
         feedbackMsg: "Hubo un error al enviar los datos. Intente de nuevo.",
       })
     )
  }

  render() {
    return (
      <section id = "telesan-hero2">
      <div className = "telesan-row">
              <div className="column">
                      <p> ¿Quieres ser parte de nuestro equipo de voluntarios?</p> 
                     

                      <form ref={this.domRef} name="Contact Form" method="POST" data-netlify="true" onSubmit={event => this.handleSubmit(event)}>
                        <input type="hidden" name="form-name" value="Contact Form" />
                        <div>
                          <label>Nombre completo:</label>
                          <input type="text" name="name" />
                        </div>
                        <div>
                          <label>Número de teléfono:</label>
                          <input type="text" name="phone" />
                        </div>
                        <div>
                          <label>Correo electrónico:</label>
                          <input type="email" name="email" />
                        </div>
                        <div>
                          <label>Area de trabajo/especialización:</label>
                          <input type="text" name="name" />
                        </div>
                        <div>
                          <label>Pequeña descripción sobre lo que haces:</label>
                          <textarea name="message" />
                        </div>

                        {this.state.feedbackMsg && <p>{this.state.feedbackMsg}</p>}
                        <button type="submit">Enviar</button>
                    </form>
              </div>
             
      </div>
  </section>
    )
  }
}

export default Formulario;